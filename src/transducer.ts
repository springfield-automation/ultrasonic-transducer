import { Sensor } from '@springfield/sensor';
import SerialPort from 'serialport';
import winston from 'winston';

export class Transducer implements Sensor<number> {
  protected log = winston.createLogger({
    transports: [
      new winston.transports.Console({
        format: winston.format.simple()
      })
    ]
  });

  private promiseResolve: ((value?: number | PromiseLike<number> | undefined) => void) | null;
  port: SerialPort;

  constructor(port: string) {
    this.promiseResolve = null;
    this.port = new SerialPort(port, { baudRate: 9600 });
    this.port.on('error', (err) => this.log.error(err.message));
    this.attachSerialPort();
  }

  private attachSerialPort() {
    const parser = this.port.pipe(new SerialPort.parsers.Readline({ delimiter: '\r'}));

    parser.on('data', (data) => {
      const index = data.lastIndexOf('R');

      if(index >= 0) {
        const value = parseInt(data.substring(index + 1));

        if(this.promiseResolve) {
          this.promiseResolve(value);
          this.promiseResolve = null;
        }
      }
    });
  }

  public getCurrentValue(): Promise<number> {
    return new Promise<number>((resolve) => {
      this.promiseResolve = resolve;
    });
  }

  public close() {
    this.port.close();
  }
}