# Overview

This module provides a sensor implementation for an ultrasonic transducer.  The transducer is expected to output values on a serial port at 9600 baud.  The data value is expected to have the format: R[number]\r.  For example: R0567\r.